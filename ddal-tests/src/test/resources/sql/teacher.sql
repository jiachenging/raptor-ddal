delete from ddal_test_0.teacher;
delete from ddal_test_1.teacher;
delete from ddal_test_2.teacher;
delete from ddal_test_3.teacher;

insert into ddal_test_1.teacher(tno,tname,sex,age,tphone) values(2012112601,'张旭','男','35','18052028779');
insert into ddal_test_1.teacher(tno,tname,sex,age,tphone) values(2012112605,'高洪泉','男','40','1310000002');
insert into ddal_test_1.teacher(tno,tname,sex,age,tphone) values(2012112609,'刘爽','女','30','18052028179');
insert into ddal_test_1.teacher(tno,tname,sex,age,tphone) values(2012112613,'刘冰','女','35','18052028179');
insert into ddal_test_1.teacher(tno,tname,sex,age,tphone) values(2012112615,'陈仓翼','女','30','1310000003');

insert into ddal_test_0.teacher(tno,tname,sex,age,tphone) values(2012112604,'王萍','女','30','18052028579');
insert into ddal_test_0.teacher(tno,tname,sex,age,tphone) values(2012112608,'李一成','男','35','18052028279');
insert into ddal_test_0.teacher(tno,tname,sex,age,tphone) values(2012112612,'李成','男','40','18052028200');
insert into ddal_test_0.teacher(tno,tname,sex,age,tphone) values(2012112616,'赵单羽','女','30','1310000003');

insert into ddal_test_2.teacher(tno,tname,sex,age,tphone) values(2012112602,'李成一','男','35','18052028279');
insert into ddal_test_2.teacher(tno,tname,sex,age,tphone) values(2012112606,'王萍','女','30','18052028579');
insert into ddal_test_2.teacher(tno,tname,sex,age,tphone) values(2012112610,'冯州龙','男','40','1310000004');
insert into ddal_test_2.teacher(tno,tname,sex,age,tphone) values(2012112614,'王萍萍','女','35','18052028579');

insert into ddal_test_3.teacher(tno,tname,sex,age,tphone) values(2012112603,'刘冰冰','女','30','18052028179');
insert into ddal_test_3.teacher(tno,tname,sex,age,tphone) values(2012112607,'张旭','男','40','18052028779');
insert into ddal_test_3.teacher(tno,tname,sex,age,tphone) values(2012112611,'教师四','女','35','1310000001');