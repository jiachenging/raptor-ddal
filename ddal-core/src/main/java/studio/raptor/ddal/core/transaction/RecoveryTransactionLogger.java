/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.core.transaction;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import studio.raptor.ddal.common.util.RuntimeUtil;

/**
 * @author Sam
 * @since 3.0.0
 */
public class RecoveryTransactionLogger extends AbstractTransactionLogger {

  private static final String TX_LOG_NAME = "ddal.transaction";
  private static final String FILE_SUFFIX = "recovery";
  private static RecoveryTransactionLogger delegate = new RecoveryTransactionLogger();
  private Logger txLogger;

  private RecoveryTransactionLogger() {
    super(RuntimeUtil.getTransactionRecordLog(), TX_LOG_NAME,
            DOT_JOINER.join(TX_LOG_NAME, FILE_SUFFIX),
            DOT_JOINER.join(TX_LOG_NAME, ARCHIVE_LAYOUT_PATTERN, FILE_SUFFIX),
            "20k");
    super.registerLogConfig();
    txLogger = LogManager.getLogger(TX_LOG_NAME);
  }

  @Override
  public void log(String message) {
    delegate.txLogger.log(Level.TRACE, message);
  }
}
