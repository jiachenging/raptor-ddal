/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.core.connection.natively;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import studio.raptor.ddal.core.connection.BackendConnection;
import studio.raptor.ddal.core.executor.resultset.ResultData;

/**
 * Native backend connection
 *
 * @author Sam
 * @since 3.0.0
 */
public class NativeBackendConnection extends BackendConnection {


  public NativeBackendConnection() {
    super();
  }

  @Override
  public void setAutoCommit(boolean autoCommit) throws SQLException {

  }

  @Override
  public boolean validate(String checkQuery, int timeout) {
    return true;
  }

  @Override
  public boolean isReadOnly() throws SQLException {
    return false;
  }

  @Override
  public void setIsReadOnly(boolean readOnly) throws SQLException {

  }

  @Override
  public void close() {

  }

  @Override
  public void reallyClose() throws SQLException {

  }

  @Override
  public void commit() throws SQLException {

  }

  @Override
  public ResultData executeQuery(String sql) throws SQLException {
    return null;
  }

  @Override
  public ResultData executeUpdate(String sql) throws SQLException {
    return null;
  }

  @Override
  public ResultData executePreparedQuery(String sql, List<Object> parameters) throws SQLException {
    return null;
  }

  @Override
  public ResultData executePreparedUpdate(String sql, List<Object> parameters) throws SQLException {
    return null;
  }

  @Override
  public void rollback() throws SQLException {

  }

  @Override
  public void setSchema(String schema) throws SQLException {

  }

  /**
   * 获取后端真是物理连接
   * 目前只有测试用到
   *
   * @return 真实物理连接
   */
  @Override
  public Connection getRealConnection() {
    return null;
  }
}
