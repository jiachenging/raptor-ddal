/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.core.transaction;

import java.util.concurrent.atomic.AtomicLong;

import studio.raptor.ddal.common.util.RuntimeUtil;

/**
 * @author Sam
 * @since 3.0.0
 */
public class DefaultTransactionIdGenerator implements TransactionIdGenerator {

  private static AtomicLong idGenerator = new AtomicLong(1L);

  /**
   * 使用系统启动时间以及自增序列保证事务的唯一性。
   *
   * 举个例子：170227041131334-6
   * ID构成解释：前半段是系统的启动时间，精确到毫秒。后半段是自增
   * 序列，保证在系统启动和关闭的运行周期内唯一。
   *
   * @return 事务ID
   */
  @Override
  public String newTransactionId() {
    return RuntimeUtil.getSysStartupTime() + "-" + idGenerator.incrementAndGet();
  }
}
