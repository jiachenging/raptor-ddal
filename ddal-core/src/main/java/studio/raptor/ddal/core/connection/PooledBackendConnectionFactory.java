/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.core.connection;

import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;

/**
 * @author Sam
 * @since 3.0.0
 */
public class PooledBackendConnectionFactory implements PooledObjectFactory<BackendConnection> {

  private PooledObjectFactory<BackendConnection> delegate;

  PooledBackendConnectionFactory(PooledObjectFactory<BackendConnection> objectFactory) {
    this.delegate = objectFactory;
  }

  @Override
  public PooledObject<BackendConnection> makeObject() throws Exception {
    return delegate.makeObject();
  }

  @Override
  public void destroyObject(PooledObject<BackendConnection> p) throws Exception {
    delegate.destroyObject(p);
  }

  @Override
  public boolean validateObject(PooledObject<BackendConnection> p) {
    return delegate.validateObject(p);
  }

  @Override
  public void activateObject(PooledObject<BackendConnection> p) throws Exception {
    delegate.activateObject(p);
  }

  @Override
  public void passivateObject(PooledObject<BackendConnection> p) throws Exception {
    delegate.passivateObject(p);
  }

  /**
   * 将连接池传递给实际的后端连接池工厂使用。
   *
   * @param poolConfig 连接池配置
   */
  void setBackendPoolConfig(BackendConnectionPoolConfig poolConfig) {
    ((AbstractPooledObjectFactory) delegate).setPoolConfig(poolConfig);
  }
}
