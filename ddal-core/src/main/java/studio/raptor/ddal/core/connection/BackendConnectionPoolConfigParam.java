/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.core.connection;

public enum BackendConnectionPoolConfigParam {

  minIdle("minIdle", true, int.class),
  maxIdle("maxIdle", true, int.class),
  maxTotal("maxTotal", true, int.class),
  timeBetweenEvictionRunsMillis("timeBetweenEvictionRunsMillis", true, long.class),
  maxWaitMillis("maxWaitMillis", true, long.class),
  testWhileIdle("testWhileIdle", true, boolean.class),
  checkQuery("checkQuery", true, String.class);

  String paramName;
  Boolean required;
  Class valueType;

  BackendConnectionPoolConfigParam(String paramName, Boolean required, Class valueType) {
    this.paramName = paramName;
    this.required = required;
    this.valueType = valueType;
  }

  public static BackendConnectionPoolConfigParam findByParamName(String paramName) {
    for (BackendConnectionPoolConfigParam each : BackendConnectionPoolConfigParam.values()) {
      if (each.paramName.equals(paramName)) {
        return each;
      }
    }
    return null;
  }
}