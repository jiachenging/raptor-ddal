/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.core.transaction;

/**
 * 事务ID生成器的抽象接口。
 * 支持本地自增序列和分布式全局序列。
 *
 * @author Sam
 * @since 3.0.0
 */
public interface TransactionIdGenerator {

  /**
   * 创建一个新的事务ID。
   * 需保证业务系统唯一性，即应用重启之后事务ID还能保持唯一性。
   *
   * @return 事务ID
   */
  String newTransactionId();
}
