/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.core.engine;

import com.google.common.hash.Hashing;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import studio.raptor.ddal.core.engine.plan.PlanNodeChain;
import studio.raptor.ddal.core.parser.result.ParseResult;

/**
 * 存储与SQL语句相关的解析结果和执行计划实例
 *
 * @author Sam
 * @since 3.0.0
 */
public class MemoryObjectsBasedOnSQL {

  private static Map<String, PlanNodeChain> planInstanceCache = new ConcurrentHashMap<>();
  private static Map<String, ParseResult> parseResultCache = new ConcurrentHashMap<>();

  public static void memorizePlan(String sql, PlanNodeChain pnc) {
    planInstanceCache.put(md5(sql), pnc);
  }

  public static PlanNodeChain getMemoryPlan(String sql) {
    return planInstanceCache.get(md5(sql));
  }

  public static void memorizeParseResult(String sql, ParseResult parseResult) {
    parseResultCache.put(md5(sql), parseResult);
  }

  public static ParseResult getMemoryParseResult(String sql) {
    return parseResultCache.get(md5(sql));
  }

  private static String md5(String sql) {
    return Hashing.md5().hashString(sql, Charset.forName("UTF-8")).toString();
  }
}