/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.core.parser.result.hint;

import java.util.ArrayList;
import java.util.List;
import studio.raptor.sqlparser.stat.TableStat.Condition;

/**
 * SQL语句解携出来的路由线索。
 * 包括隐藏分片字段，指定分片，读写连接选择控制
 *
 * @author Sam
 * @since 3.0.0
 */
public class HintContext {

    /**
     * 隐藏的分片键值对。
     */
    private List<Condition> hintShardConditions = new ArrayList<>();
    private boolean hasHiddenCondition = false;

    /**
     * 指定分片值
     */
    private String sharding;
    private boolean hasSharding = false;

    /**
     * 指定读写连接， 如果sql注释中没有指定读写模式，该字段必须为空
     */
    private ReadWriteMode readWriteMode = null;

    public List<Condition> getHintShardConditions() {
        return hintShardConditions;
    }

    public void addHintShardConditions(Condition hintShardCondition) {
        this.hintShardConditions.add(hintShardCondition);
        this.hasHiddenCondition = true;
    }

    public boolean hasHiddenCondition() {
        return hasHiddenCondition;
    }

    public String getSharding() {
        return sharding;
    }

    public void setSharding(String sharding) {
        this.sharding = sharding;
        this.hasSharding = true;
    }

    public boolean hasSharding() {
        return hasSharding;
    }

    public ReadWriteMode getReadWriteMode() {
        return readWriteMode;
    }

    public void setReadWriteMode(ReadWriteMode readWriteMode) {
        this.readWriteMode = readWriteMode;
    }

    @Override
    public String toString() {
        return "HintContext{" +
                "hintShardConditions=" + hintShardConditions +
                ", sharding='" + sharding + '\'' +
                ", readWriteMode=" + readWriteMode +
                '}';
    }
}
