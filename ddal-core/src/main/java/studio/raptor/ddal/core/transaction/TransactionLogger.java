/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.core.transaction;

/**
 * 目前事务日志有两个实现，分别时主线事务日志（{@link MainLineTransactionLogger}）
 * 和恢复事务日志{@link RecoveryTransactionLogger}。 主线事务日志负责对业务过程
 * 产生的事务进行记录，而恢复事务日志，故名思议负责检查和恢复异常日志，在系统启动时创建，
 * 恢复工作完成之后即刻销毁。
 *
 * @author Sam
 * @since 3.0.0
 */
interface TransactionLogger {

  void log(String message);
}
