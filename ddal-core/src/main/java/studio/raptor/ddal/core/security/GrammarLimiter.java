package studio.raptor.ddal.core.security;

import studio.raptor.ddal.config.model.shard.Table;
import studio.raptor.ddal.config.model.shard.VirtualDb;
import studio.raptor.ddal.core.parser.result.ParseResult;

/**
 * SQL语法限制器
 *
 * @author Charley
 * @since 1.0
 */
public class GrammarLimiter {

  public static void check(ParseResult parseResult, VirtualDb virtualDb) throws RuntimeException{
    switch(parseResult.getSqlType()){
      case UPDATE:
        //校验Update语句的Set字段是否包含分片字段
        if(!updateItemVerify(parseResult, virtualDb)){
          //TODO 异常处理
          throw new RuntimeException("Shard column can't be updated.");
        }
        break;
      default:
        break;
    }
  }

  /**
   * update语法校验
   *
   * 校验Update语句的Set字段是否包含分片字段
   * @param parseResult
   * @param virtualDb
   * @return  若存在则返回false，不存在返回true
   */
  public static boolean updateItemVerify(ParseResult parseResult, VirtualDb virtualDb){
    boolean isSuccess = true;
    String tableName = parseResult.getTableNames().iterator().next();
    Table table = virtualDb.getTable(tableName);
    for(String item : parseResult.getUpdateItems()){
      for(String shardColumn : table.getShardColumns()){
        isSuccess = !shardColumn.equals(item);
      }
    }
    return isSuccess;
  }
}
