/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.config.model.shard;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import studio.raptor.ddal.common.exception.GenericException;
import studio.raptor.ddal.common.util.StringUtil;
import studio.raptor.ddal.config.exception.ConfigErrCodes;

public class VirtualDb {

  private String name;
  private boolean rmOwner;
  private int sqlMaxLimit;
  private String shardGroup;
  private Shards shards;
  private Tables tables;
  private Seqs seqs;

  /**
   * 缓存应用SQL中的表名与其大小写的映射关系，避免每次都做大小写转换
   */
  private static transient Map<String, String> ucase0 = new ConcurrentHashMap<>();

  public VirtualDb() {
  }

  public Table getTable(String tableName) {
    String ucaseTableName = ucase0.get(tableName);
    if (StringUtil.isEmpty(ucaseTableName)) {
      ucaseTableName = tableName.toUpperCase();
      ucase0.put(tableName, ucaseTableName);
    }
    Table table = tables.get(ucaseTableName);
    if (null == table) {
      throw new GenericException(ConfigErrCodes.CONFIG_109, tableName);
    }
    return table;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isRmOwner() {
    return rmOwner;
  }

  public void setRmOwner(boolean rmOwner) {
    this.rmOwner = rmOwner;
  }

  public int getSqlMaxLimit() {
    return sqlMaxLimit;
  }

  public void setSqlMaxLimit(int sqlMaxLimit) {
    this.sqlMaxLimit = sqlMaxLimit;
  }

  public VirtualDb(String name, boolean rmOwner, int sqlMaxLimit, Shards shards, Tables tables,
      Seqs seqs) {
    super();
    this.name = name;
    this.rmOwner = rmOwner;
    this.sqlMaxLimit = sqlMaxLimit;
    this.shards = shards;
    this.tables = tables;
    this.seqs = seqs;
  }

  public String getShardGroup() {
    return shardGroup;
  }

  public void setShardGroup(String shardGroup) {
    this.shardGroup = shardGroup;
  }

  public Shards getShards() {
    return shards;
  }

  public void setShards(Shards shards) {
    this.shards = shards;
  }

  public Tables getTables() {
    return tables;
  }

  public void setTables(Tables tables) {
    this.tables = tables;
  }

  public Seqs getSeqs() {
    return seqs;
  }

  public void setSeqs(Seqs seqs) {
    this.seqs = seqs;
  }

  public static class Seq {

    private String name;
    private String type;
    private String incr;
    private String start;
    private String cache;

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getType() {
      return type;
    }

    public void setType(String type) {
      this.type = type;
    }

    public String getIncr() {
      return incr;
    }

    public void setIncr(String incr) {
      this.incr = incr;
    }

    public String getStart() {
      return start;
    }

    public void setStart(String start) {
      this.start = start;
    }

    public String getCache() {
      return cache;
    }

    public void setCache(String cache) {
      this.cache = cache;
    }

    @Override
    public String toString() {
      return "Seq [name=" + name + ", type=" + type + ", incr=" + incr + ", start=" + start
          + ", cache=" + cache
          + "]";
    }

  }

  public static class Seqs extends ArrayList<Seq> {

    private static final long serialVersionUID = -5368444663469003836L;
  }

  @Override
  public String toString() {
    return "VirtualDb [name=" + name + ", rmOwner=" + rmOwner + ", sqlMaxLimit=" + sqlMaxLimit
        + ", tables="
        + tables + ", seqs=" + seqs + "]";
  }

}
