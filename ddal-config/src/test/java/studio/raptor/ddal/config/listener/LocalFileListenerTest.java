/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.config.listener;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

/**
 * 功能描述
 *
 * @author Charley
 * @since 1.0
 */
public class LocalFileListenerTest {

  public static void main(String[] args) {
    try {
      Path myDir = Paths
          .get("/Users/Charley/Work/Idea/raptor/raptor-ddal/ddal-config/target/test-classes/");
      WatchService watcher = myDir.getFileSystem().newWatchService();
      myDir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE,
          StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
      WatchKey watckKey = watcher.take();
      List<WatchEvent<?>> events = watckKey.pollEvents();
      for (WatchEvent event : events) {
        if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
          System.out.println("Created: " + event.context().toString());
        }
        if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE) {
          System.out.println("Delete: " + event.context().toString());
        }
        if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
          System.out.println("Modify: " + event.context().toString());
        }
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
