package studio.raptor.ddal.demo.jmeter.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.sql.DataSource;

/**
 * 功能描述
 *
 * @author Charley
 * @since 1.0
 */
public class TestTask implements Runnable {

  private final String sql = "select CNO, CNAME, TNO from course where cno = ?";
  private final DataSource dataSource;

  public TestTask(DataSource dataSource){
    this.dataSource = dataSource;
  }

  /**
   * When an object implementing interface <code>Runnable</code> is used
   * to create a thread, starting the thread causes the object's
   * <code>run</code> method to be called in that separately executing
   * thread.
   * <p>
   * The general contract of the method <code>run</code> is that it may
   * take any action whatsoever.
   *
   * @see Thread#run()
   */
  @Override
  public void run() {
    while(true){
      try(
          Connection conn = dataSource.getConnection();
          PreparedStatement preparedStatement = conn.prepareStatement(sql);
      ){
        preparedStatement.setInt(1, 300004);
        try(
            ResultSet resultSet = preparedStatement.executeQuery()
        ){}
      }catch (Exception e){
        e.printStackTrace();
      }
    }
  }
}
