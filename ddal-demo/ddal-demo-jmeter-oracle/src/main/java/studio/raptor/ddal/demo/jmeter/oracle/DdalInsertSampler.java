/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.demo.jmeter.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.concurrent.atomic.AtomicLong;
import javax.sql.DataSource;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import studio.raptor.ddal.jdbc.RaptorDataSource;

/**
 * @author Sam
 * @since 3.0.0
 */
public class DdalInsertSampler extends AbstractJavaSamplerClient {

  private static DataSource dataSource;
  private static final AtomicLong ID = new AtomicLong(System.currentTimeMillis());
  private static final String INSERT_SQL = "insert into T_FATIGUE_TESTING (id, c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16) "
      + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
  private DataSource getDataSource() {
    if (null == dataSource) {
      synchronized (this) {
        if (null == dataSource) {
          dataSource = new RaptorDataSource("school", "mysql");
        }
      }
    }
    return dataSource;
  }

  @Override
  public SampleResult runTest(JavaSamplerContext javaSamplerContext) {
    SampleResult sampleResult = new SampleResult();
    sampleResult.sampleStart();
    try (
        Connection conn = getDataSource().getConnection();
        PreparedStatement statement = conn.prepareStatement(INSERT_SQL);
    ) {
      statement.setLong(1, ID.getAndIncrement());
      statement.setString(2, RandomStringUtils.randomAlphabetic(100));
      statement.setString(3, RandomStringUtils.randomAlphabetic(100));
      statement.setString(4, RandomStringUtils.randomAlphabetic(100));
      statement.setString(5, RandomStringUtils.randomAlphabetic(100));
      statement.setString(6, RandomStringUtils.randomAlphabetic(100));
      statement.setString(7, RandomStringUtils.randomAlphabetic(100));
      statement.setString(8, RandomStringUtils.randomAlphabetic(100));
      statement.setString(9, RandomStringUtils.randomAlphabetic(100));
      statement.setString(10, RandomStringUtils.randomAlphabetic(100));
      statement.setString(11, RandomStringUtils.randomAlphabetic(100));
      statement.setString(12, RandomStringUtils.randomAlphabetic(100));
      statement.setString(13, RandomStringUtils.randomAlphabetic(100));
      statement.setString(14, RandomStringUtils.randomAlphabetic(100));
      statement.setString(15, RandomStringUtils.randomAlphabetic(100));
      statement.setString(16, RandomStringUtils.randomAlphabetic(100));
      statement.setString(17, RandomStringUtils.randomAlphabetic(100));
      statement.execute();
      conn.commit();
      sampleResult.setSuccessful(true);
    } catch (Exception e) {
      e.printStackTrace();
      sampleResult.setSuccessful(false);
    }
    sampleResult.sampleEnd();
    return sampleResult;
  }
}