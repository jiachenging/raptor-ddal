package studio.raptor.ddal.demo.jmeter.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import javax.sql.DataSource;
import studio.raptor.ddal.jdbc.RaptorDataSource;

/**
 * @author Sam
 * @since 3.0.0
 */
public class TestMain {
  private static final AtomicLong ID = new AtomicLong(System.currentTimeMillis());
  private DataSource dataSource = new RaptorDataSource("school", "oracle");

  private static ExecutorService executorService = Executors.newFixedThreadPool(20);

  public void runTest(){
    for(int i=0; i<20; i++){
      TestTask task = new TestTask(dataSource);
      executorService.submit(task);
    }

    executorService.shutdown();
  }

  public static void main(String[] args) throws SQLException {
    //new TestMain().runTest();
    DataSource dataSource = new RaptorDataSource("school", "oracle");
    try (
        Connection conn = dataSource.getConnection();
        PreparedStatement statement = conn.prepareStatement(JMeterConstants.INSERT);
    ) {
      statement.setLong(1, ID.getAndIncrement());
      int resultSet = statement.executeUpdate();
      conn.commit();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
