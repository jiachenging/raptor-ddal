/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.common.exception;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Sam
 * @since 3.0.0
 */
public class ExecuteException extends RuntimeException {

  private Code code;
  private String message;

  ExecuteException(Code code) {
    this(code, null);
  }

  ExecuteException(Code code, String message) {
    this.code = code;
    this.message = message;
  }
  /**
   * Read the error Code for this exception
   * @return the error Code for this exception
   */
  public Code code() {
    return code;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public static ExecuteException create(Code code) {
    switch (code) {
      case READ_WRITE_STRATEGY_CONFIG_ERROR:
        return new ReadWriteStrategyCfgErrException();
      case READ_WRITE_STRATEGY_INSTANCE_ERROR:
        return new ReadWriteStrategyInstanceException();
      case GET_READONLY_CONNECTION_FAILED_ERROR:
        return new GetReadOnlyConnectionFailedException();
      case GET_READWRITE_CONNECTION_FAILED_ERROR:
        return new GetReadWriteConnectionFailedException();
      case EXECUTION_ERROR_ON_PHYSICAL_DB:
        return new ExecutionErrorOnPhysicalDBException();
      default:
        throw new IllegalArgumentException("Invalid exception code");
    }
  }

  public static ExecuteException create(Code code, String message) {
    ExecuteException ee = create(code);
    ee.setMessage(message);
    return ee;
  }

  public enum Code {

    // 读写控制策略配置异常
    READ_WRITE_STRATEGY_CONFIG_ERROR(-127),
    // 读写控制策略实例化异常
    READ_WRITE_STRATEGY_INSTANCE_ERROR(-126),
    // 获取只读连接失败
    GET_READONLY_CONNECTION_FAILED_ERROR(-125),
    // 获取读写连接失败
    GET_READWRITE_CONNECTION_FAILED_ERROR(-124),
    // sql 执行异常
    EXECUTION_ERROR_ON_PHYSICAL_DB(-123),
    ;

    private static final Map<Integer,ExecuteException.Code> lookup = new HashMap<>();

    static {
      for(ExecuteException.Code c : EnumSet.allOf(ExecuteException.Code.class)) {
        lookup.put(c.code, c);
      }
    }

    private final int code;
    Code(int code) {
      this.code = code;
    }

    /**
     * Get the int value for a particular Code.
     * @return error code as integer
     */
    public int intValue() { return code; }

    /**
     * Get the Code value for a particular integer error code
     * @param code int error code
     * @return Code value corresponding to specified int code, or null
     */
    public static ExecuteException.Code get(int code) {
      return lookup.get(code);
    }
  }

  public static class ReadWriteStrategyCfgErrException extends ExecuteException {
    ReadWriteStrategyCfgErrException() {
      super(Code.READ_WRITE_STRATEGY_CONFIG_ERROR);
    }
  }

  public static class ReadWriteStrategyInstanceException extends ExecuteException {
    ReadWriteStrategyInstanceException() {
      super(Code.READ_WRITE_STRATEGY_INSTANCE_ERROR);
    }
  }

  public static class GetReadOnlyConnectionFailedException extends ExecuteException {
    GetReadOnlyConnectionFailedException() {
      super(Code.GET_READONLY_CONNECTION_FAILED_ERROR);
    }
  }

  public static class GetReadWriteConnectionFailedException extends ExecuteException {
    GetReadWriteConnectionFailedException() {
      super(Code.GET_READWRITE_CONNECTION_FAILED_ERROR);
    }
  }

  public static class ExecutionErrorOnPhysicalDBException extends ExecuteException {
    ExecutionErrorOnPhysicalDBException() {
      super(Code.EXECUTION_ERROR_ON_PHYSICAL_DB);
    }
  }
}
