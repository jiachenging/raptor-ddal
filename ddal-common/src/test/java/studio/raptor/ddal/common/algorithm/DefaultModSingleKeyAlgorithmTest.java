/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.common.algorithm;

/**
 * @author Sam
 * @since 3.0.0
 */
public class DefaultModSingleKeyAlgorithmTest extends AbstractSingleKeyShardAlgorithmTest {

//  private static SingleKeyShardAlgorithm<Long> shardAlgorithm = new DefaultModSingleKeyAlgorithm(
//      allShards.size() + "");
//
//
//  @Test
//  public void testDoEqual() {
//    Assert.assertThat(shardAlgorithm.doEqual(allShards, new ShardValue<>("columnName", 99L)),
//        Is.is("shard3"));
//    Assert.assertThat(shardAlgorithm.doEqual(allShards, new ShardValue<>("columnName", 100L)),
//        Is.is("shard0"));
//    Assert.assertThat(shardAlgorithm.doEqual(allShards, new ShardValue<>("columnName", 101L)),
//        Is.is("shard1"));
//  }
//
//  @Test(expected = UnsupportedOperationException.class)
//  public void testDoEqualNoShardFound() {
//    Collection<String> allShards = new ArrayList<String>() {
//      {
//        add("shard0");
//        add("shard1");
//        add("shard2");
//        add("shard2");
//      }
//    };
//    String actualShard = shardAlgorithm.doEqual(allShards, new ShardValue<>("columnName", 3L));
//    Assert.assertThat("shard3", Is.is(actualShard));
//  }
//
//  @Test
//  public void testDoIn() {
//    String expectedArray = "[shard0, shard1]";
//    List<String> actualArray = new ArrayList<>(shardAlgorithm
//        .doIn(allShards, new ShardValue<>("columnName", Arrays.asList(1L, 100L, 5L))));
//    Collections.sort(actualArray);
//    Assert.assertThat(actualArray.toString(), Is.is(expectedArray));
//  }
//
//  @Test
//  public void testDoBetween1() {
//    String expectedArray = "[shard0, shard1, shard2, shard3]";
//    List<String> actualArray = new ArrayList<>(
//        shardAlgorithm.doBetween(allShards, new ShardValue<>("columnName", Range.closed(5L, 10L))));
//    Collections.sort(actualArray);
//    Assert.assertThat(actualArray.toString(), Is.is(expectedArray));
//  }
//
//  @Test
//  public void testDoBetween2() {
//    String expectedArray = "[shard0, shard1]";
//    List<String> actualArray = new ArrayList<>(
//        shardAlgorithm.doBetween(allShards, new ShardValue<>("columnName", Range.closed(4L, 5L))));
//    Collections.sort(actualArray);
//    Assert.assertThat(actualArray.toString(), Is.is(expectedArray));
//  }
}
