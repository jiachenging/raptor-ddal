/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package studio.raptor.ddal.common.bytes;

import java.util.concurrent.TimeUnit;
import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test cases for ByteUtil
 *
 * @author Sam
 * @since 3.0.0
 */
public class ByteUtilTest {

  @Test
  public void testTimeStringMethods() {
    Assert.assertThat(ByteUtil.timeStringAs("1s", TimeUnit.MILLISECONDS), Is.is(1000L));
    Assert.assertThat(ByteUtil.timeStringAsMs("1s"), Is.is(1000L));
    Assert.assertThat(ByteUtil.timeStringAsSec("1s"), Is.is(1L));
  }

  @Test
  public void testByteStringAsMethods() {
    Assert.assertThat(ByteUtil.byteStringAs("1024k", ByteUnit.MiB), Is.is(1L));
    Assert.assertThat(ByteUtil.byteStringAsBytes("1K"), Is.is(1024L));
    Assert.assertThat(ByteUtil.byteStringAsKb("1m"), Is.is(1024L));
    Assert.assertThat(ByteUtil.byteStringAsMb("1g"), Is.is(1024L));
    Assert.assertThat(ByteUtil.byteStringAsGb("1t"), Is.is(1024L));
  }

  @Test
  public void testBytesToString() {
    Assert.assertThat(ByteUtil.bytesToString(1024), Is.is("1024.0 B"));
    Assert.assertThat(ByteUtil.bytesToString(1024 * 1024), Is.is("1024.0 KB"));
    Assert.assertThat(ByteUtil.bytesToString(1024 * 1024 * 1024), Is.is("1024.0 MB"));
    Assert.assertThat(ByteUtil.bytesToString(1024 * 1024 * 1024 * 1024L), Is.is("1024.0 GB"));
    Assert
        .assertThat(ByteUtil.bytesToString(1024 * 1024 * 1024L * 1024 * 1024L), Is.is("1024.0 TB"));
  }

  //Time must be specified as seconds (s), " +
  //               "milliseconds (ms), microseconds (us), minutes (m or min), hour (h), or day (d). " +
  //                     "E.g. 50s, 100ms, or 250us.
  @Test(expected = NumberFormatException.class)
  public void testTimeStringException1() {
    Assert.assertThat(ByteUtil.timeStringAs("s", TimeUnit.MILLISECONDS), Is.is(1000L));
  }

  //Failed to parse time string:
  @Test(expected = NumberFormatException.class)
  public void testTimeStringException2() {
    Assert.assertThat(ByteUtil.timeStringAs("1s1", TimeUnit.MILLISECONDS), Is.is(1000L));
  }

  //Invalid suffix:
  @Test(expected = NumberFormatException.class)
  public void testTimeStringException3() {
    Assert.assertThat(ByteUtil.timeStringAs("1y", TimeUnit.MILLISECONDS), Is.is(1000L));
  }


  //Time must be specified as seconds (s), " +
  //               "milliseconds (ms), microseconds (us), minutes (m or min), hour (h), or day (d). " +
  //                     "E.g. 50s, 100ms, or 250us.
  @Test(expected = NumberFormatException.class)
  public void testByteStringException1() {
    Assert.assertThat(ByteUtil.byteStringAs("g", ByteUnit.GiB), Is.is(1000L));
  }

  //Failed to parse time string:
  @Test(expected = NumberFormatException.class)
  public void testByteStringException2() {
    Assert.assertThat(ByteUtil.byteStringAs("1g1", ByteUnit.GiB), Is.is(1000L));
  }

  //Invalid suffix:
  @Test(expected = NumberFormatException.class)
  public void testByteStringException3() {
    Assert.assertThat(ByteUtil.byteStringAs("1o", ByteUnit.GiB), Is.is(1000L));
  }

  //Fractional values are not supported.
  @Test(expected = NumberFormatException.class)
  public void testByteStringException4() {
    Assert.assertThat(ByteUtil.byteStringAs("10.24k", ByteUnit.MiB), Is.is(1L));
  }
}
